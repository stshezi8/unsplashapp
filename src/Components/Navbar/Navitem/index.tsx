import { Flex } from "@chakra-ui/react";
import React from "react";

export const Navitem: React.FC<{
  title: string;
  onClick: () => void;
}> = ({  title, onClick }) => {
  return (
    <Flex onClick={onClick}  justifyContent="left"  alignItems="center" mb="2" mt="2" ml="2" mr="2" p="2"
      _hover={{
        background: "grey",
        cursor: "pointer",
        color: "white",
      }}
    >
      {title}
    </Flex>
  );
};
