import React from "react";
import { Box,Button,Drawer,DrawerContent,DrawerOverlay,Heading,SimpleGrid,useDisclosure} from "@chakra-ui/react";
import axios from "axios";
import { useQuery } from "react-query";
import { Navitem } from "./Navitem";

export const Navbar: React.FC<{
  setTopic: React.Dispatch<React.SetStateAction<string | null>>;
}> = ({ setTopic }) => {
  const { onOpen, isOpen, onClose } = useDisclosure();
  const { data, isLoading } = useQuery({
    queryKey: "topics",
    queryFn: () =>
      axios.get("topics", {
        headers: {
          Authorization: "Client-ID " + process.env.REACT_APP_UNSPLASH_SECRET,
        },
      }),
  });

  return (
    
    <SimpleGrid h={"10vh"} columns={2} backgroundColor="#D3D3D3">
      
      <Box display="flex" alignItems="center" justifyContent="left">
        {!isLoading && (
          <Button backgroundColor="transparent" color="black" marginLeft="1.5vw"  size="lg"  border="solid 1px white" onClick={onOpen}>
            Topics
          </Button>
        )}
      </Box>

      
      <Box display="flex" alignItems="center"  justifyContent="left">
        <Heading  color="black">
          Unsplash Topics
        </Heading>
      </Box>

      
      <Drawer  autoFocus={true}  isOpen={isOpen} placement="left"  onClose={onClose}  returnFocusOnClose={true}  onOverlayClick={onClose} size={"xs"}>
        <DrawerOverlay />
        <DrawerContent>
          <Box height="10vh" display="flex" alignItems="center" justifyContent="center" backgroundColor="#D3D3D3" >
            <Heading color="black" size="md"> Select a topic: </Heading>
          </Box>
          
          {data?.data.map((topic: any) => (
            <Navitem  key={topic?.title} title={topic?.title} onClick={() => setTopic(topic?.slug)} />
          ))}

        </DrawerContent>
      </Drawer>
    </SimpleGrid>
  );
};
